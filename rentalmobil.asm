.model small
.code
Org 100h
Start:
	jmp mulai
	nama db 13,10,'Nama anda       : $'
	hp db 13,10,'No.HP/Telp      : $'
	alamat db 13,10,'Alamat          : $'
	berangkat_dari db 13,10,'Berangkat Dari  : $'
 	Kota_Tujuan db 13,10,'Tujuan          : $'
	lanjut db 13,10,'Lanjut Tekan Y untuk Lanjut ---> $'
  
   tampung_nama db 30,?,30 dup(?)
   tampung_hp 13,?,13 dup(?)
   tampung_alamat 13,?,13 dup(?)
   tampung_berangkat 13,?,13 dup(?)
   tampung_tujuan 13,?,13 dup(?)

   masukkan db 13,10,' Silahkan Masukan Kode Mobil Yang Anda Pilih : $'

daftar db 13,10,'*---*----------------------*-------------------*--------------*'
   db 13,10,'|			RENTAL MOBIL 				|'
   db 13,10,'*---*----------------------*-------------------*--------------*'
   db 13,10,'|No |	Jenis Mobil	| Harga (Tarif)     | Tambah Supir |'
   db 13,10,'*---*----------------------*-------------------*--------------*'
   db 13,10,'|1 |	Mobil Jazz	  | Rp. 500.000/hari  | Rp. 550.000  |'
   db 13,10,'*---*----------------------*-------------------*--------------*'
   db 13,10,'|2 |	Mobil Brio	  | Rp. 400.000/hari  | Rp. 450.000  |'
   db 13,10,'*---*----------------------*-------------------*--------------*'
   db 13,10,'|3 |	Mobil Alpart    | Rp. 1000.000/hari | Rp. 1050.000 |'
   db 13,10,'*---*----------------------*-------------------*--------------*'
   db 13,10,'|4 |	Mobil Pajero    | Rp. 800.000 /hari | Rp. 850.000  |'
   db 13,10,'*---*----------------------*-------------------*--------------*'
   db 13,10,'|5 |	Mobil Avanza    | Rp. 300.000 /hari | Rp. 350.000  |' 
   db 13,10,'*---*----------------------*-------------------*--------------*','$'
mulai:
	mov ah,09h
	lea dx,nama
	int 21h
	mov ah,0ah
	lea dx,tampung_nama
	int 21h
	push dx


	mov ah,09h
	lea dx,hp
	int 21h
	mov ah,0ah
	lea dx,tampung_hp
	int 21h
	push dx


	mov ah,09h
	lea dx,alamat
	int 21h
	mov ah,0ah
	lea dx,tampung_alamat
	int 21h
	push dx


	mov ah,09h
	lea dx,berangkat_dari
	int 21h
	mov ah,0ah
	lea dx,tampung_berangkat
	int 21h
	push dx


	mov ah,09h
	lea dx,kota_tujuan
	int 21h
	mov ah,0ah
	lea dx,tampung_tujuan
	int 21h
	push dx


	mov ah,09h
	 mov dx,offset daftar
	 int 21h
	 mov ah,09h
	 mov dx,offset lanjut
	 int 21h
	 mov ah,01h
	 int 21h
	 cmp al,'y'
	 jmp proses
	 jne error_msg
error_msg:
	mov ah,09h
	mov dx,offset error_msg
	int 21h
	int 20h
proses:
	mov ah,09h
	mov dx,offset masukkan
	int 21h
	

	mov ah,01h
	int 21h
	mov bh,al


	cmp al,'1'
	je hasil1

	cmp al,'2'
	je hasil2

	cmp al,'3'
	je hasil3

	cmp al,'4'
	je hasil4
	
	cmp al,'5'
	je hasil5

hasil1:
	mov ah,09h
	lea dx,teks1
	int 21h
	int 20h

hasil2:
	mov ah,09h
	lea dx,teks2
	int 21h
	int 20h

hasil3:
	mov ah,09h
	lea dx,teks3
	int 21h
	int 20h
	
hasil4:
	mov ah,09h
	lea dx,teks4
	int 21h
	int 20h

	
hasil5:
	mov ah,09h
	lea dx,teks5
	int 21h
	int 20h

teks1  db 13,10,'Anda memilih Mobil Jazz'
db 13,10,'Dengan catatan memakai sopir atau tidak'
db 13,10,'Total Harga yang anda bayar : Rp. 500.000 (tidak memakai sopir) Rp. 550.000 (dengan supir)'
db 13,10,'Terima Kasih $'

teks2  db 13,10,'Anda memilih Mobil Brio'
db 13,10,'Dengan catatan memakai sopir atau tidak'
db 13,10,'Total Harga yang anda bayar : Rp. 400.000 (tidak memakai sopir) Rp. 450.000 (dengan supir)'
db 13,10,'Terima Kasih $'

teks3  db 13,10,'Anda memilih Mobil Alpart'
db 13,10,'Dengan catatan memakai sopir atau tidak'
db 13,10,'Total Harga yang anda bayar : Rp. 1000.000 (tidak memakai sopir) Rp. 1050.000 (dengan supir)'
db 13,10,'Terima Kasih $'

teks4  db 13,10,'Anda memilih Mobil Pajero'
db 13,10,'Dengan catatan memakai sopir atau tidak'
db 13,10,'Total Harga yang anda bayar : Rp. 800.000 (tidak memakai sopir) Rp. 850.000 (dengan supir)'
db 13,10,'Terima Kasih $' 


teks5  db 13,10,'Anda memilih Mobil Avanza'
db 13,10,'Dengan catatan memakai sopir atau tidak'
db 13,10,'Total Harga yang anda bayar : Rp. 300.000 (tidak memakai sopir) Rp. 350.000 (dengan supir)'
db 13,10,'Terima Kasih $'
	

end Start
	
